# CSE 331

Please make sure to address tasks in new branches. Do not push to master pls and ty.

## Dependencies ##

### Construct ###

https://construct.readthedocs.io/en/latest/intro.html
```
pip install construct
```
There are no hard dependencies, but if you would like to install all supported (not required) modules listed on the Construct installation page, you can use the 2nd command-line form.
```
pip install construct[extras]
```

## Run Code ##

```
python ./main.py `user-agent` `isDFS` `depth` `num_to_crawl`
```