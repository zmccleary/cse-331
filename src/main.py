'''
DO NOT IMPLEMENT ANY OF THE UNDERLYING LOGIC IN HERE. #CLEANCODE
'''
import argparse
from crawler import Crawler
from http_request import HTTPRequest
from search import DFS, BFS

filters = set()

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('starting_link', help='Starting point of Crawler')
  parser.add_argument('--user_agent', type=str, help='Custom user agent for HTTP request headers')
  parser.add_argument('--dfs', action='store_true', help='Use DFS instead of BFS to search')
  parser.add_argument('--max_depth', type=int, help='Max depth of crawler')
  parser.add_argument('--max_pages', type=int, help='Max pages to crawl')
  parser.add_argument('--stopwords', action='store_true', help='Include stopwords')
  parser.add_argument('--permutations', '-p', action='store_true',help='Generates all permutations of capitalization for words. Thorough but takes significantly longer')
  parser.add_argument('--verbose', '-v', action='store_true', help='Prints HTTP requests as they are sent')

  args = parser.parse_args()

  if (args.stopwords):
    data = []
    with open('./resources/filters.csv') as file:
      data += file.readline().split(',')

    for word in data:
      filters.add(word)
    filters.add(',')

  if args.dfs:
    search = DFS(args.max_pages, args.max_depth)
  else:
    search = BFS(args.max_pages, args.max_depth)

  request = HTTPRequest(args.user_agent)
  crawler = Crawler(args.starting_link, search, filters, request, args.permutations, args.verbose)
  crawler.crawl()
  response = crawler.brute_force()
  print(response)
