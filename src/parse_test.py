from bs4 import BeautifulSoup
from crawler import Crawler
filters = set()
data = []
sampleHtmlPath="../resources/projectHtml.htm"
htmlContent=""

with open('../resources/filters.csv') as file:
    data += file.readline().split(',')
with open(sampleHtmlPath) as testFile:
    htmlContent= testFile.read()

for word in data:
    filters.add(word)

crawler = Crawler(None, None, filters, None)
possible = (BeautifulSoup(htmlContent, 'html.parser'))
