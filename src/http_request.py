import socket
import ssl
from urllib.parse import urlparse
class HTTPRequest:
  @staticmethod
  def open_socket_and_connect(scheme, netloc):
    if scheme == 'https':
      context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
      s = context.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), server_hostname=netloc)
      port = 443
    elif scheme == 'http':
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      port = 80
    return s
    


  def __init__(self, user_agent):
    self.user_agent = user_agent

  def parse_header(self, header):
    header = header.split('\r\n')
    status = int(header[0].split(' ')[1])
    #print(f"status: {status}")
    if status >= 400:
      return None
    attrs = header[1:-2]
    header_dict = {}
    for attr in attrs:
      vals = attr.split(': ')
      header_dict[vals[0]] = vals[1]
    return header_dict

  def get(self, url,verbose):
    parsed_url = urlparse(url)
    scheme = parsed_url.scheme
    netloc = parsed_url.netloc
    path = '/' if parsed_url.path == '' else parsed_url.path
    request = f'GET {path} HTTP/1.1\r\nHost: {netloc}\r\n'
    if self.user_agent is not None:
      request += f'User-Agent: {self.user_agent}\r\n\r\n'
    else:
      request += '\r\n'
    request = request.encode('utf-8')
    
    if scheme == 'https':
      context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
      s = context.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), server_hostname=netloc)
      port = 443
    elif scheme == 'http':
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      port = 80
    '''
    try:
      s.connect((netloc, port))
    except:
      return None
    '''
    while True:
      try:
        try:
          s.connect((netloc, port))
          if verbose:
            print(request.decode('utf-8'))
          s.send(request)
  
        except:
          return None
        header = s.recv(1).decode('utf-8')
        while not header.endswith('\r\n\r\n'):
          header += s.recv(1).decode('utf-8')
        if verbose:
          print(header)

        header_dict = self.parse_header(header)
        if header_dict is None:
          return None
        content_type = 'text/plain' if 'Content-Type' not in header_dict else header_dict['Content-Type']
        content_length = int(header_dict['Content-Length']) if 'Content-Length' in header_dict else None
        if content_length is None or (not content_type.startswith('text/html') and not content_type.startswith('text/plain')):
          return None
        response = b''
        while content_length > 0:
          temp_response = s.recv(content_length)
          content_length -= len(temp_response)
          response += temp_response
      except ConnectionResetError:
        pass
      else:
        break
    return response.decode('utf-8')

  def post(self, url, verbose, **kwargs):
    parsed_url = urlparse(url)
    scheme = parsed_url.scheme
    netloc = parsed_url.netloc
    path = '/' if parsed_url.path == '' else parsed_url.path
    messages = [] 
    for key,value in kwargs.items():
      messages.append(f'{key}={value}')
    message_body = '&'.join(messages)
    #print(message_body)

    request = f'POST {path} HTTP/1.1\r\nHost: {netloc}\r\nContent-Type: application/x-www-form-urlencoded\r\ncache-control: no-cache\r\n'

    if self.user_agent is not None:
      request += f'User-Agent: {self.user_agent}\r\n'
    #else:
    #  request += '\r\n'
    request +=f'Content-Length: {len(message_body)}\r\n\r\n'
    request += f'{message_body}'
    request = request.encode('utf-8')
    if scheme == 'https':
      context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
      s = context.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), server_hostname=netloc)
      port = 443
    elif scheme == 'http':
      s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      port = 80
    
    while True:
      try:
        try:
          s.connect((netloc, port))
          if verbose:
            print(request.decode('utf-8'))
          s.send(request)
        except:
          return None
        header = s.recv(1).decode('utf-8')
        while not header.endswith('\r\n\r\n'):
          header += s.recv(1).decode('utf-8')
        if verbose:
           print(header)

        header_dict = self.parse_header(header)
        # print(header_dict)
        if header_dict is None:
          return None
        content_type = 'text/plain' if 'Content-Type' not in header_dict else header_dict['Content-Type']
        content_length = int(header_dict['Content-Length']) if 'Content-Length' in header_dict else None
        if content_length is None or (not content_type.startswith('text/html') and not content_type.startswith('text/plain')):
          return None
        response = b''
        while content_length > 0:
          temp_response = s.recv(content_length)
          content_length -= len(temp_response)
          response += temp_response
        if verbose:
          print(response)
      except ConnectionResetError:
        s.close()
        pass
      else:
        break
    return response.decode('utf-8')
  
  