from http_request import HTTPRequest

def http_request_test(req):
  print(req.get('http://example.com'))

def https_request_test(req):
  print(req.get('https://www.cnn.com/'))

if __name__ == '__main__':
  req = HTTPRequest('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36')
  http_request_test(req)
  https_request_test(req)