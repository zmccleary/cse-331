from collections import deque
from urllib.parse import urljoin, urlparse, urlunparse

class Link:
  subdomains = set()
  with open('./resources/subdomains.csv') as file:
    line = file.readline().split(',')
    subdomains = set(line)

  def __init__(self, link, depth):
    self.link = link
    self.depth = depth

  @staticmethod
  def get_absolute_link(target_link, link_to_append):
    link = urljoin(target_link.link, link_to_append)
    if urlparse(target_link.link).netloc != urlparse(link).netloc:
      return None
    return Link(link, target_link.depth + 1)

  def replace_subdomain(self, new_subdomain):
    parsed_url = urlparse(self.link)
    netloc = parsed_url.netloc.split('.')
    if netloc[0] in Link.subdomains:
      netloc[0] = new_subdomain
    else:
      netloc.insert(0, new_subdomain)
    parsed_url = parsed_url._replace(netloc='.'.join(netloc))
    return Link(urlunparse(parsed_url), self.depth)

  def __repr__(self):
    return self.link

class Search:
  def __init__(self, pages, depth):
    self.links = deque()
    self.visited = set()
    self.max_pages = pages
    self.max_depth = depth

  def has_next(self):
    return len(self.links) > 0

  def get_next(self):
    return self.links.popleft()

  def discard_link(self):
    if self.max_pages is not None:
      self.max_pages += 1

  def get_url_without_trailing_slash_and_scheme(self, link):
    url = link.link.replace('https://', '')
    url = url.replace('http://', '')
    if url[-1] == '/':
      url = url[:-1]
    return url

  def contains_link(self, link):
    raw_url = self.get_url_without_trailing_slash_and_scheme(link)
    index_html_raw_url = raw_url + '/index.html'
    curr_urls = map(lambda x: self.get_url_without_trailing_slash_and_scheme(x), self.links)
    return raw_url in curr_urls or index_html_raw_url in curr_urls

  def add_visited(self, link):
    self.visited.add(self.get_url_without_trailing_slash_and_scheme(link))

  def finished_visit(self, link):
    raw_url = self.get_url_without_trailing_slash_and_scheme(link)
    index_html_raw_url = raw_url + '/index.html'
    return raw_url in self.visited or index_html_raw_url in self.visited

  def __repr__(self):
    return str(self.links)

class DFS(Search):
  def add(self, link):
    if not self.contains_link(link) and not self.finished_visit(link) and (self.max_pages is None or self.max_pages > 0) and (self.max_depth is None or self.max_depth >= link.depth):
      self.links.appendleft(link)
      if self.max_pages is not None:
        self.max_pages -= 1

class BFS(Search):
  def add(self, link):
    if not self.contains_link(link) and not self.finished_visit(link) and (self.max_pages is None or self.max_pages > 0) and (self.max_depth is None or self.max_depth >= link.depth):
      self.links.append(link)
      if self.max_pages is not None:
        self.max_pages -= 1