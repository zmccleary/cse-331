leet_conversions = {
  'a': '4',
  'e': '3',
  'l': '1',
  't': '7',
  'o': '0'
}

'''
Generates all possible leetspeak, capitalized, and lowercase permutations for a word
'''
def to_leet_helper(word):
  if word[0] in leet_conversions:
    sub_leet = leet_conversions[word[0]] + word[0] + word[0].upper()
  else:
    sub_leet = word[0] + word[0].upper()

  if len(word[1:]) > 0:
    perms = [s + p for s in sub_leet for p in to_leet_helper(word[1:])]
  else:
    perms = sub_leet
  
  return perms

def to_leet_simple(word):
  if word[0] in leet_conversions:
    sub_leet = leet_conversions[word[0]] + word[0]
  else:
    sub_leet = word[0]
  if len(word[1:]) > 0:
    perms = [s + p for s in sub_leet for p in to_leet_simple(word[1:])]
  else:
    perms = sub_leet
  return perms
'''
Returns all permutations for a word and their reversed versions
'''
def to_leet(word, perm):
  if len(word) <= 1:
      return [word]
    
  if perm:
    perms = to_leet_helper(word.lower())
  else:
    perms = to_leet_simple(word.lower())
  return set(perms[:] + [perm[::-1] for perm in perms] + [perm[:].upper() for perm in perms])

def from_leet(word):
  word = word.lower()
  reverse_conversions = {value: key for key,value in leet_conversions.items()}
  for k,v in reverse_conversions.items():
    word = word.replace(k,v)
  return word